#!/usr/bin/python2

import argparse 
import numpy as np

from get_features import *
from sql import *

parser = argparse.ArgumentParser()
parser.add_argument('img', type=str, help="Input image.")
args = parser.parse_args()

bgr_img = cv2.imread(args.img)
if bgr_img is None:
    raise Exception("Unable to load image: {}".format(args.img))
rgb_img = cv2.cvtColor(bgr_img, cv2.COLOR_BGR2RGB)

face = get_face(rgb_img)
index = 0.0

for i in range(128):
    index =+ face[i]

taxicab_result = search(face, index)

for result in taxicab_result:
    array = np.zeros(128)
    for x in range(128):
        array[x] = result[x]
    array -= face
    if np.dot(array,array) < 1.0:
        print(result[128])
