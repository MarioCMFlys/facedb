# RENAME THIS FILE TO "config.py" AND MAKE YOUR CHANGES THERE


##  Database

# IP / DNS to connect to database server. Port optional
host = "127.0.0.1"
# Database username. 
user = "faces"
# Database password.
pswd = "some_pass"
# Name of database.
database = "faces"


##  Facial Recognition

# AlignDLib Facial Landmarks
align = "/opt/shape_predictor_68_face_landmarks.dat"
# TorchNeuralNet file
net = "/opt/nn4.small2.v1.t7"
# Temporary files directory
temp = "/tmp"
