#!/usr/bin/python2

import config
import mysql.connector

mydb = mysql.connector.connect(
    host=config.host, user=config.user, passwd=config.pswd, database=config.database
)

cursor = mydb.cursor()

face_query = "INSERT IGNORE INTO faces VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) ON DUPLICATE KEY UPDATE date=NULL"


def add_face(face, x1, y1, x2, y2, url, index):
    print(url)
    cursor.execute(face_query, (map(str, face) + [ str(url), x1, y1, x2, y2, str(index), None ]))
    mydb.commit()

def search(face, index):
    query = "SELECT * FROM faces WHERE power(ind - {},2) < 0.5;".format(index)
    cursor.execute(query)
    return cursor.fetchall()
