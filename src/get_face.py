#!/usr/bin/python2

import argparse

from get_features import *

parser = argparse.ArgumentParser()
parser.add_argument('img', type=str, help="Input image url.")
args = parser.parse_args()
get_faces(args.img)
