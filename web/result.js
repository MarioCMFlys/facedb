$(document).ready(function(){
  $('#fileupload').fileupload({
    url: '/cgi-bin/upload.py',
    dataType: 'json',
    done: function (e, data) {
      console.log(data);
    },
    progressall: function (e, data) {
      console.log(data);
    }
  }).prop('disabled', !$.support.fileInput)
    .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
